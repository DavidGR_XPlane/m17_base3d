using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyHitboxController : MonoBehaviour, IDamageable, IPushable
{
    public event Action<int> OnDamage;
    [Min(0f)]
    public float DamageMultiplier = 1f;

    public void Damage(int amount)
    {
        Debug.Log($"{gameObject} impactat per un valor de {amount * DamageMultiplier} punts de mal.");
        OnDamage?.Invoke((int)(amount * DamageMultiplier));
    }

    public void Push(Vector3 direction, float impulse)
    {
        GetComponent<Rigidbody>().AddForce(direction * impulse * DamageMultiplier, ForceMode.Impulse);
    }
}
