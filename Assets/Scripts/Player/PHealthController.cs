using System;
using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;

public class PHealthController : MonoBehaviour, IDamageable
{
    [SerializeField] private GameEvent OnPlayerDeath;
    [SerializeField] public HealthSO HealthSO;

    public event Action<int> OnDamage;

    private void Start()
    {
        HealthSO.health = 100;
    }

    public void Damage(int amount)
    {
        HealthSO.health -= amount;
        CheckIfDead();
    }

    private void CheckIfDead()
    {
        if (HealthSO.health <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        OnPlayerDeath.Raise();
    }

}
