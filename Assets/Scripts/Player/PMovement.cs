using System.Collections;
using System.Collections.Generic;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.Assertions;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Controls;
using UnityEngine.Windows;

public class PMovement : MonoBehaviour
{
    [Header("Params")]
    [SerializeField]
    private float m_Sensitivity;
    [SerializeField]
    private float m_Speed;
    [SerializeField]

    [Header("Input")]
    private InputActionAsset p_Input;
    [DoNotSerialize] public InputAction m_CameraAction;
    private InputAction m_MovementAction;
    private InputActionMap gameplay;

    [Header("Components")]
    private Rigidbody m_Rigidbody;
    private Transform m_Transform;
    private PController m_PController;
    private PShootController m_ShootController;
    private PCameraController m_CameraController;

    private void Awake()
    {
        InitComponents();
        SubscribeInputBindings();
    }

    private void Update()
    {
        PlayerMovement();
    }

    private void SubscribeInputBindings()
    {
        Cursor.lockState = CursorLockMode.Locked;
        p_Input = Instantiate(m_PController.p_InputAsset);
        gameplay = p_Input.FindActionMap("Game");
        m_MovementAction = gameplay.FindAction("Movement");
        m_CameraAction = gameplay.FindAction("Camera");
        gameplay.FindAction("ShootType1").started += m_ShootController.Shoot1;
        gameplay.FindAction("ShootType2").started += m_ShootController.Shoot2;
        gameplay.FindAction("SwitchCamera").started += m_CameraController.SwitchCamera;
        gameplay.Enable();
    }

    private void PlayerMovement()
    {
        Vector3 m_Forward = m_Transform.forward * m_MovementAction.ReadValue<Vector2>().y;
        Vector3 m_Side = m_Transform.right * m_MovementAction.ReadValue<Vector2>().x;
        m_Rigidbody.velocity = (m_Forward + m_Side).normalized * m_Speed + Vector3.up * m_Rigidbody.velocity.y;
    }

    private void InitComponents()
    {
        m_Rigidbody = GetComponent<Rigidbody>();
        m_Transform = GetComponent<Transform>();
        m_PController = GetComponent<PController>();
        m_ShootController = GetComponent<PShootController>();
        m_CameraController = GetComponent<PCameraController>();
    }

    public void UNSubscribeInputBindings()
    {
        gameplay.FindAction("ShootType1").started -= m_ShootController.Shoot1;
        gameplay.FindAction("ShootType2").started -= m_ShootController.Shoot2;
        gameplay.FindAction("SwitchCamera").started -= m_CameraController.SwitchCamera;
        gameplay.Disable();
    }

}
