using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.ShaderGraph.Internal;
using UnityEngine;
using UnityEngine.InputSystem;

public class PCameraController : MonoBehaviour
{
    [Header("Params")]
    [SerializeField]
    private float m_Sensitivity;

    [Header("Components")]
    private PMovement m_Movement;

    [Header("Camera")]
    [SerializeField]
    public Camera m_FirstPersonCamera;
    public Camera m_ThirdPersonCamera;
    public Camera m_ActiveCamera;
    private float camPitch = 0;

    private bool FirstPerson;

    private void Awake()
    {
        InitComponents();
    }

    private void Start()
    {
        FirstPerson = true;
        InitFirstPersonCamera();
    }

    private void Update()
    {
        PlayerCameraMovement();
    }

    public void Recoil(float amount)
    {
        RotateCameraX(amount);
    }

    private void RotateCameraX(float amount)
    {
        camPitch += amount;
        if (m_ActiveCamera.Equals(m_FirstPersonCamera))
            camPitch = Mathf.Clamp(camPitch, -75, 75);
        else if (m_ActiveCamera.Equals(m_ThirdPersonCamera))
            camPitch = Mathf.Clamp(camPitch, -30, -13);
        m_ActiveCamera.transform.localEulerAngles = new Vector3(-camPitch, 0, 0);
    }

    private void PlayerCameraMovement()
    {
        //Player Camera
        Vector2 mouseVector = m_Sensitivity * Time.deltaTime * m_Movement.m_CameraAction.ReadValue<Vector2>();
        transform.Rotate(Vector3.up * mouseVector.x);
        //m_ActiveCamera.transform.Rotate(Vector3.left * mouseVector.y);
        RotateCameraX(mouseVector.y);
    }

    public void SwitchCamera(InputAction.CallbackContext context)
    {
        if (FirstPerson)
            InitThirdPersonCamera();
        else
            InitFirstPersonCamera();
    }

    private void InitFirstPersonCamera()
    {
        m_ThirdPersonCamera.enabled = false;
        m_ThirdPersonCamera.gameObject.SetActive(false);
        m_FirstPersonCamera.gameObject.SetActive(true);
        m_FirstPersonCamera.enabled = true;
        FirstPerson = true;
        m_ActiveCamera = m_FirstPersonCamera;
    }

    private void InitThirdPersonCamera()
    {

        m_FirstPersonCamera.enabled = false;
        m_FirstPersonCamera.gameObject.SetActive(false);
        m_ThirdPersonCamera.gameObject.SetActive(true);
        m_ThirdPersonCamera.enabled = true;
        FirstPerson = false;
        m_ActiveCamera = m_ThirdPersonCamera;
    }

    private void InitComponents()
    {
        m_Movement = GetComponent<PMovement>();
    }

}