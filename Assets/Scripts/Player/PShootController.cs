using System.Collections;
using UnityEngine;
using UnityEngine.InputSystem;

public class PShootController : MonoBehaviour
{
    [Header("Shoot Layer")]
    [SerializeField]
    private LayerMask m_ShootMask;

    [Header("Checks")]
    [SerializeField] private bool m_IsShooting;

    [Header("Components")]
    private PCameraController m_CameraController;

    private void Awake()
    {
        InitComponents();
        m_IsShooting = false;
    }

    public void Shoot1(InputAction.CallbackContext context)
    {
        //
        if (m_IsShooting)
            return;
        m_IsShooting = true;
        print("TYPE 1 SHOOTING - PISTOL");
        RaycastHit hit;
        Transform cameraTransform = m_CameraController.m_ActiveCamera.transform;
        Vector3 direction = cameraTransform.forward;
        Vector3 DispersionX = cameraTransform.right * Random.Range(-0.03f, 0.03f);
        Vector3 DispersionY = cameraTransform.up * Random.Range(-0.03f, 0.03f);
        direction += DispersionX + DispersionY;
        direction.Normalize();

        if (Physics.Raycast(cameraTransform.transform.position, direction, out hit, 20f, m_ShootMask))
        {
            m_CameraController.Recoil(Random.Range(1f, 2f));
            Debug.Log($"He tocat {hit.collider.gameObject} a la posici� {hit.point} amb normal {hit.normal}");
            Debug.DrawLine(cameraTransform.position, hit.point, Color.magenta, 2f);
            if (hit.collider.TryGetComponent<IDamageable>(out IDamageable target))
            {
                print("HOLAAAAAAAAAAAAAAAAAAAAA");
                target.Damage(10);
            }
            if (hit.collider.TryGetComponent<IPushable>(out IPushable pushable))
                pushable.Push(m_CameraController.m_ActiveCamera.transform.forward, 10);
        }
        m_IsShooting = false;
    }

    public void Shoot2(InputAction.CallbackContext context)
    {
        if (m_IsShooting)
            return;
        StartCoroutine(SchweizerOrgeli());
    }

    public void Shoot2Base()
    {
        print("TYPE 2 SHOOTING - SPECIAL SHOTGUN");
        for (int i = 0; i < 10; i++)
        {
            RaycastHit hit;
            Transform cameraTransform = m_CameraController.m_FirstPersonCamera.transform;
            Vector3 direction = cameraTransform.forward;
            Vector3 DispersionX = cameraTransform.right * Random.Range(-0.1f, 0.1f);
            Vector3 DispersionY = cameraTransform.up * Random.Range(-0.1f, 0.1f);
            direction += DispersionX + DispersionY;
            direction.Normalize();
            if (Physics.Raycast(cameraTransform.position, direction, out hit, 20f, m_ShootMask))
            {
                Debug.Log($"He tocat {hit.collider.gameObject} a la posici� {hit.point} amb normal {hit.normal}");
                Debug.DrawLine(m_CameraController.m_FirstPersonCamera.transform.position, hit.point, Color.yellow, 2f);
                for (int j = 0; j < 5; j++)
                    cameraTransform.transform.localEulerAngles += cameraTransform.right * Random.Range(1f, 2f);
                if (hit.collider.TryGetComponent<IDamageable>(out IDamageable target))
                    target.Damage(20);
                if (hit.collider.TryGetComponent<IPushable>(out IPushable pushable))
                    pushable.Push(m_CameraController.m_ActiveCamera.transform.forward, 10);
            }
        }
    }

    IEnumerator SchweizerOrgeli()
    {
        m_IsShooting = true;
        Camera camera = m_CameraController.m_ActiveCamera.GetComponent<Camera>();
        float originalFov = camera.fieldOfView;
        for (int i = 0; i < 10; i++)
        {
            camera.fieldOfView += 3.5f;
            yield return new WaitForSeconds(0.3f);
        }
        Shoot2Base();
        while (camera.fieldOfView > originalFov)
        {
            camera.fieldOfView -= 3.5f;
            if (camera.fieldOfView < originalFov)
                camera.fieldOfView = originalFov;
            yield return new WaitForSeconds(0.1f);
        }
        m_IsShooting = false;
    }

    private void InitComponents()
    {
        m_CameraController = GetComponent<PCameraController>();
    }

}
