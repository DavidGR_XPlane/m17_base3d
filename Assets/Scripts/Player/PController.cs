
using UnityEngine;
using UnityEngine.InputSystem;

public class PController : MonoBehaviour
{
    [Header("Input")][SerializeField] public InputActionAsset p_InputAsset;

    private Light Light;

    private void Awake()
    {
        Light = GetComponentInChildren<Light>();
    }

    public void RedLight()
    {
        Light.color = Color.red;
    }

    public void whiteLight()
    {
        Light.color = Color.white;
    }


}
