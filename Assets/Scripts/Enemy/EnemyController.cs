using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class EnemyController : MonoBehaviour
{
    private GameObject m_Target;

    [Header("Waypoints")]
    [SerializeField] private List<GameObject> m_Targets;

    [Header("Events")]
    [SerializeField] private GameEvent PursuitPlayer;
    [SerializeField] public GameEvent UnPursuitPlayer;

    [Header("Components")]
    private NavMeshAgent m_Agent;
    private Animator m_Animator;
    private RagdollController m_RagdollController;

    [Header("DetectionAreas")]
    [SerializeField] private EnemyTriggerController m_PlayerDetectionArea;
    [SerializeField] private EnemyTriggerController m_AttackDetectionArea;

    [Header("LayerMasks")]
    [SerializeField] private LayerMask m_ShootLayerMask;

    [Header("Checks")]
    [SerializeField] private bool isTargetingPlayer;
    [SerializeField] private bool isDead;

    [Header("Events")]
    [SerializeField] private GameEvent GUIUpdate;

    /* STATE MACHINE */

    [Header("State Machine")]
    [SerializeField] private SwitchMachineStates m_CurrentState;

    private enum SwitchMachineStates { NONE, IDLE, PATROL, CHASE, HIT1, LOOKING, DEAD };

    private void Awake()
    {
        isTargetingPlayer = false;
        m_Agent = GetComponent<NavMeshAgent>();
        m_Animator = GetComponent<Animator>();
        m_RagdollController = GetComponent<RagdollController>();
        m_PlayerDetectionArea.OnEnter += TargetPlayer;
        m_PlayerDetectionArea.OnExit += UntargetPlayer;
        m_AttackDetectionArea.OnEnter += AttackPlayer;
        m_AttackDetectionArea.OnExit += TargetPlayer;
    }

    private void Start()
    {
        ChangeState(SwitchMachineStates.PATROL);
        UpdateDestination();
    }

    void Update()
    {
        CheckIfTouchingWaypoint();
        UpdateState();

        if (Input.GetKey(KeyCode.P))
        {
            ChangeState(SwitchMachineStates.DEAD);
        }
    }

    public void Die()
    {
        m_Animator.enabled = false;
        m_Agent.enabled = false;
        m_PlayerDetectionArea.OnEnter -= TargetPlayer;
        m_PlayerDetectionArea.OnExit -= UntargetPlayer;
        m_AttackDetectionArea.OnEnter -= AttackPlayer;
        m_AttackDetectionArea.OnExit -= TargetPlayer;
        ChangeState(SwitchMachineStates.DEAD);
    }

    void AttackPlayer(GameObject target)
    {
        ChangeState(SwitchMachineStates.HIT1);
    }

    void Shoot()
    {
        print("shoot");
        print("soc un enemic y et dispar");
        RaycastHit hit;
        Vector3 direction = transform.forward;
        Vector3 DispersionX = transform.right * Random.Range(-0.1f, 0.1f);
        Vector3 DispersionY = transform.up * Random.Range(-0.1f, 0.1f);
        direction += DispersionX + DispersionY;
        direction.Normalize();
        if (Physics.Raycast(transform.position, direction, out hit, 20f, m_ShootLayerMask))
        {
            transform.localEulerAngles += -transform.right * Random.Range(1f, 2f);
            Debug.Log($"He tocat {hit.collider.gameObject} a la posici� {hit.point} amb normal {hit.normal}");
            Debug.DrawLine(transform.position, hit.point, Color.magenta, 2f);
            if (hit.collider.TryGetComponent<IDamageable>(out IDamageable target))
                target.Damage(10);
            GUIUpdate.Raise();
        }
    }

    void CheckIfTouchingWaypoint()
    {
        if (isTargetingPlayer)
            return;
        {
            if (Vector3.Distance(transform.position, m_Target.transform.position) < 1)
                UpdateDestination();
        }
    }

    void UpdateDestination()
    {
        m_Target = m_Targets[RandomWaypointIndex()];
        m_Agent.SetDestination(m_Target.transform.position);
    }

    void TargetPlayer(GameObject target)
    {
        PursuitPlayer.Raise();
        isTargetingPlayer = true;
        m_Target = target;

        // Calcular la direcci�n hacia el jugador
        Vector3 directionToPlayer = target.transform.position - transform.position;

        // Calcular la direcci�n opuesta
        Vector3 oppositeDirection = -directionToPlayer.normalized;

        // Determinar una posici�n alejada del jugador en la direcci�n opuesta
        float fleeDistance = 10.0f; // Distancia que queremos que el agente huya
        Vector3 fleePosition = transform.position + oppositeDirection * fleeDistance;

        // Establecer la posici�n de destino en la direcci�n opuesta
        m_Agent.SetDestination(fleePosition);

        // Cambiar el estado a CHASE (aunque el nombre del estado puede ser confuso si est� huyendo)
        ChangeState(SwitchMachineStates.CHASE);

    }

    //void Chase()
    //{
    //    if (!isTargetingPlayer)
    //    {
    //        ChangeState(SwitchMachineStates.CHASE);
    //    }
    //}

    void UntargetPlayer(GameObject target)
    {
        UnPursuitPlayer.Raise();
        ChangeState(SwitchMachineStates.LOOKING);
    }

    IEnumerator LookForPlayer()
    {

        yield return new WaitForSeconds(12);
        ChangeState(SwitchMachineStates.PATROL);
    }

    int RandomWaypointIndex()
    {
        return Random.Range(0, m_Targets.Count);
    }


    private void ChangeState(SwitchMachineStates newState)
    {
        if (newState == m_CurrentState)
            return;
        ExitState();
        InitState(newState);
    }

    private void InitState(SwitchMachineStates currentState)
    {
        if (m_CurrentState == SwitchMachineStates.DEAD)
            return;
        m_CurrentState = currentState;
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                m_Animator.Play("Idle");
                break;
            case SwitchMachineStates.PATROL:
                m_Animator.Play("Walk");
                m_Agent.isStopped = false;
                break;
            case SwitchMachineStates.CHASE:
                m_Animator.Play("Chase");
                m_Agent.isStopped = false;
                break;
            case SwitchMachineStates.HIT1:
                m_Animator.Play("HitPhoneTalking");
                m_Agent.isStopped = true;
                break;
            case SwitchMachineStates.LOOKING:
                m_Animator.Play("Looking");
                isTargetingPlayer = false;
                m_Target = m_Targets[RandomWaypointIndex()];
                m_Agent.isStopped = true;
                StartCoroutine(LookForPlayer());
                break;
            case SwitchMachineStates.DEAD:
                break;
            default:
                break;
        }
    }

    private void ExitState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                break;
            case SwitchMachineStates.PATROL:
                break;
            case SwitchMachineStates.CHASE:
                break;
            case SwitchMachineStates.HIT1:
                break;
            case SwitchMachineStates.LOOKING:
                break;
            default:
                break;
        }
    }

    private void UpdateState()
    {
        switch (m_CurrentState)
        {
            case SwitchMachineStates.IDLE:
                break;
            case SwitchMachineStates.PATROL:
                break;
            case SwitchMachineStates.CHASE:
                m_Agent.SetDestination(m_Target.transform.position);
                break;
            case SwitchMachineStates.HIT1:
                break;
            case SwitchMachineStates.LOOKING:
                break;
            default:
                break;
        }
    }
}