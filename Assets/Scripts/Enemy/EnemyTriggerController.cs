using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyTriggerController : MonoBehaviour
{
    public event Action<GameObject> OnEnter;
    public event Action<GameObject> OnExit;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
            OnEnter?.Invoke(other.gameObject);
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Player"))
            OnExit?.Invoke(other.gameObject);
    }
}
