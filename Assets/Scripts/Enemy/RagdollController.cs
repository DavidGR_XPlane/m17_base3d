using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RagdollController : MonoBehaviour
{

    private Rigidbody[] m_Bones;
    private EnemyController m_EnemyController;
    private Animator m_Animator;
    [SerializeField] private int m_Health;

    private void Awake()
    {
        m_Health = 100;
        m_Animator = GetComponent<Animator>();
        m_Bones = GetComponentsInChildren<Rigidbody>();
        m_EnemyController = GetComponent<EnemyController>();
        Activate(false);
        foreach (Rigidbody bone in m_Bones)
            if (bone.TryGetComponent<IDamageable>(out IDamageable damageable))
                damageable.OnDamage += ReceiveDamage;
    }



    private void ReceiveDamage(int damage)
    {
        print("I received " + damage + " damage.");
        m_Health -= damage;
        if (m_Health <= 0)
        {
            Die();
        }
    }


    public void Die()
    {
        GetComponent<EnemyController>().UnPursuitPlayer.Raise();
        foreach (Rigidbody bone in m_Bones)
            if (bone.TryGetComponent<IDamageable>(out IDamageable damageable))
                damageable.OnDamage -= ReceiveDamage;
        m_EnemyController.Die();
        Activate(true);
    }

    public void Activate(bool state)
    {
        foreach (Rigidbody bone in m_Bones)
            bone.isKinematic = !state;
        m_Animator.enabled = !state;
    }
}
