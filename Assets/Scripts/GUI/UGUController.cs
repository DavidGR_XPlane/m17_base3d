
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class UGUController : MonoBehaviour
{

    [SerializeField] private TextMeshProUGUI healthText;
    [SerializeField] private HealthSO playerHealth;

    private void Start()
    {
        ShowHp();
    }

    public void ShowHp()
    {
        healthText.text = " " + playerHealth.health;
    }

}
