using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TurretDectionArea : MonoBehaviour
{

    public Action<GameObject> OnPlayerEnter;
    public Action OnPlayerExit;

    private void OnTriggerStay(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            OnPlayerEnter?.Invoke(other.gameObject);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            OnPlayerExit?.Invoke();
        }
    }
}
