using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public class TurretController : MonoBehaviour
{
    public TurretDectionArea TurretDectionArea;
    public LayerMask layerMask;
    public bool shooting = false;
    private Coroutine shootingCouroutine;


    private void Awake()
    {
        TurretDectionArea.OnPlayerEnter += Aim;
        TurretDectionArea.OnPlayerExit += StopShooting;
    }

    private void Aim(GameObject player)
    {
        transform.LookAt(player.transform.position);
        //transform.Rotate(90, 0, 0);
        if (shooting) return;

        shootingCouroutine = StartCoroutine(ShootRay());
    }

    private void StopShooting()
    {
        if (shootingCouroutine != null) { StopCoroutine(shootingCouroutine); }
        shooting = false;
    }

    private IEnumerator ShootRay()
    {
        shooting = true;
        while (shooting)
        {
            if (Physics.Raycast(transform.position, transform.forward, out RaycastHit hit, 20f, layerMask))
            {
                Debug.DrawLine(transform.position, hit.point, Color.green, 20f);
                print("ShootRay");
                if (hit.collider.TryGetComponent(out IDamageable target))
                {
     
                    print("TURRET HAS HIT PLAYER!");
                    target.Damage(20);
                }

            }
            yield return new WaitForSeconds(1);
        }
    }

    private void OnDestroy()
    {
        TurretDectionArea.OnPlayerEnter -= Aim;
        TurretDectionArea.OnPlayerExit -= StopShooting;
    }

}
