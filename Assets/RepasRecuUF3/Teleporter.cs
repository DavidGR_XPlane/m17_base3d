using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Teleporter : MonoBehaviour
{
    [SerializeField] private Transform targetTeleporter;
    public float teleportCooldown = 1.0f;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Player"))
        {
            other.transform.position = targetTeleporter.position + new Vector3(1.5f, 1, 1.5f);
        }
    }




}
